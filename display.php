<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="CSS/ass2style.css">
	<head>
		<title>CSC3426 Assignment 2 Matthew Stallan</title>
	</head>
	<body>
	<div id='container'>
	<nav>
		<!-- navigation Bar -->
		<ul>
  			<li><a href="WaterHockey.html">Home</a></li>
  			<li class="dropdown">
   				 <a class="dropbtn">Teams</a>
    			<div class="dropdown-content">
     				<a href="display.php?team=Ridgebacks">Ridgebacks</a>
     				<a href="display.php?team=Chermside%20Turtles">Chermside Turtles</a>
     			 	<a href="display.php?team=Jindalee%20Sharks">Jindalee Sharks</a>
     			 	<a href="display.php?team=Fortitude%20Fish">Fortitude Fish</a>
     			 	<a href="display.php?team=Paddington%20Porpoises">Paddington Porpoises</a>
     			 	<a href="display.php?team=The%20Wild%20Dugongs">The Wild Dugongs</a>
   				</div>
  			</li>
  		</ul>
	</nav>
		<?php
		$db = new SQLite3('Ass2db.db');
		if( !array_key_exists('team', $_GET)){
		// if team hasnt been selected display list of all teams/manangers/pools
			echo "<h1 class=\"title\">Teams</h1>";
			$query = "SELECT t.TeamName as Team,t.Manager, t.TeamPool as 'Home Pool', p.PoolAddress as Address FROM TeamTable as t, PoolTable as p WHERE t.TeamPool = p.TeamPool;";
			$results=$db->query($query);
			
			$cols = $results->numColumns();
			echo "<table>";
			//loop to print table
			for($j=0; $j<$cols; $j++){
					echo"<th>".$results->columnName($j)."</th>";
			}
				echo "<tr>";
				while($row = $results->fetchArray(SQLITE3_NUM)){
					for ($i = 0; $i < $cols; $i++) {
						if($i==0){
							echo"<td>";
							echo "<a href='display.php?team=$row[$i]'>".htmlspecialchars($row[$i])."</a>";
         		    		echo"</td>";
         		    	}else{
         		    		echo"<td>";
							echo"$row[$i]";
         		    		echo"</td>"; 
         		   		}
         		   	}
         	   		echo "</tr>"; 
			
				}
			echo"</table>";	
		}else if( array_key_exists('team', $_GET)){
			// if specific team selected diplay playeer info for that team
			echo "<h1 class=\"title\">$_GET[team]</h1>";
			$team = SQLite3::escapeString($_GET['team']);
			$query = "SELECT Manager FROM TeamTable WHERE TeamName = '$team';";
			$results=$db->query($query);
			$row = $results->fetchArray(SQLITE3_NUM);
			echo "<div class='manager'><h3 class='managerH'>Manager:</h3><p class='managerP'> " . $row[0] . "</p></div>";
			$query = "SELECT PID,PlayerGivenName ||' '|| PlayerLastName, PlayerDoB,Handed FROM PlayerTable WHERE TeamName ='$team';";
			$results=$db->query($query);
			$cols = $results->numColumns();
			echo "<table>";
			echo"<tr><th>"."Player ID</th><th>Name</th><th>Date Of Birth</th></th><th>Handed</th></tr>";
			echo "<tr>";
			// loop to print player info
			while($row = $results->fetchArray(SQLITE3_NUM)){
				for ($i = 0; $i < $cols; $i++) {
					if($i==2){
						echo"<td>";
						$date = $row[$i];
						echo DateTime::createFromFormat('Ymd', $date)->format('d-m-Y');
         		   		echo"</td>"; 
         		 	}else{
         		 		echo"<td>";
						echo "$row[$i]";
         		   		echo"</td>";
         			}
         		}
				echo "</tr>"; 
			}
			echo"</table><button class='right'><a href='display.php'>Back</a></button>";	
			
		}
		?>	
		</div>
	</body>
</html>
