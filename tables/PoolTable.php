<style>
<?php include '../CSS/ass2style.css'; ?>
</style>

<?php
	$db = new SQLite3('../Ass2db.db');
	$table= "PoolTable";
		//check if user wants to delete row
	if(array_key_exists('delete1',$_GET)&&array_key_exists('delete2',$_GET)){
		echo"<div id='container'>";
		$delete1 = SQLite3::escapeString($_GET['delete1']);
		$delete2 = SQLite3::escapeString($_GET['delete2']);
		echo"<h1>Delete Record?</h1>";
		//select record user wants to delete
		$query = "SELECT * FROM $table WHERE TeamPool = '$delete1' AND PoolAddress = '$delete2'";
		$results = $db->query($query);
		$cols = $results->numColumns();
		echo "<table>";
		echo"<tr>";
		//display record user wants to delete
		for($j=0; $j<$cols; $j++){
			echo"<th>".$results->columnName($j)."</th>";
		}
		echo "<tr>";
		while($row = $results->fetchArray(SQLITE3_NUM)){
			for ($i = 0; $i <$cols; $i++) {
	      		echo"<td>";
				echo"$row[$i]";
	       		echo"</td>"; 
	       		}
	         	   	
	         		
	       	echo "</tr>"; 
			
			}
		echo"</table>";
		echo"<div class=\"bucket\">";
		// form for confirming deletion or canceling
		echo"<p class=\"center\">Are you sure you want to delete this record?</p>";
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<button class=\"left\" type=\"submit\" name=\"delete\" value=\"Submit\">Yes</button>";
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";
		echo"</div>";
	}
	// check if user wants to edit record
	if(array_key_exists('edit1',$_GET)&&array_key_exists('edit2',$_GET)){
		echo"<div id='container'>";
		$edit1 = SQLite3::escapeString($_GET['edit1']);
		$edit2 = SQLite3::escapeString($_GET['edit2']);
		//select record user wants to edit
		$query = "SELECT * FROM $table WHERE TeamPool = '$edit1' AND PoolAddress = '$edit2'";	
		$results = $db->query($query);
		$cols = $results->numColumns();
		$row = $results->fetchArray(SQLITE3_NUM);
		// display form with selected records data to allow editing or cancel
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<h1>Edit Game</h1>";
		echo"<div class=\"bucket\">";
		for($i=0;$i<$cols;$i++){
			if($results->columnName($i) == "PoolLength"){
				echo"<p><label>".$results->columnName($i).": <input type=\"number\" name=\"".$results->columnName($i)."\" value=\"".$row[$i]."\"required='required' pattern=\"[0-9]+\"/></label></p>";
			}else{
				echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\""
				.$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'/>
				</label></p>";
			}
		}
		echo"<button class=\"left\" type=\"submit\" name=\"edit\" value=\"Submit\">Edit</button>";
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";	
		echo"</div>";
		
			
	}else if(!(array_key_exists('edit1',$_GET)||array_key_exists('edit2',$_GET)) 
			&& !(array_key_exists('delete1',$_GET)||array_key_exists('delete2',$_GET))){	
		
		$query = "SELECT * FROM $table";
		$results = $db->query($query);
		$cols = $results->numColumns();
		//form to add new record to table
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<div class=\"bucket\">";
		echo"<h3>Add New Game</h3>";
		for($i=0;$i<$cols;$i++){
			if($results->columnName($i) == "PoolLength"){
				echo"<p><label>".$results->columnName($i).": <input type=\"number\" name=\"".$results->columnName($i)."\" value=\"".$row[$i]."\"required='required' pattern=\"[0-9]+\"/></label></p>";
			}else{
				echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\""
				.$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'/>
				</label></p>";
			}
		}
		echo"<button class=\"add\" type=\"submit\" name=\"add\" value=\"Submit\">Add</button>";
		echo"</form>";
		echo"</div>";
	}
	//sql for adding new record
	if (isset($_POST['add'])){
	//get data for new record
		$teamPool = SQLite3::escapeString(htmlspecialchars($_POST['TeamPool']));
		$pAddress = SQLite3::escapeString(htmlspecialchars($_POST['PoolAddress']));
		$pLength = SQLite3::escapeString(htmlspecialchars($_POST['PoolLength']));
		// prepare statement for adding new record
		$query = $db->prepare("INSERT INTO $table(TeamPool,PoolAddress,PoolLength)
								VALUES(:teamPool,:pAddress,:pLength)");
		$query->bindValue(":teamPool",$teamPool);
		$query->bindValue(":pAddress",$pAddress);
		$query->bindValue(":pLength",$pLength);
		$query->execute();
		header("Location:addDelete.php?table=$table");
	}
	//sql for editing record	
	if(isset($_POST['edit'])){
	//get data for edited record
		$teamPool = SQLite3::escapeString(htmlspecialchars($_POST['TeamPool']));
		$pAddress = SQLite3::escapeString(htmlspecialchars($_POST['PoolAddress']));
		$pLength = SQLite3::escapeString(htmlspecialchars($_POST['PoolLength']));
		$pLength = (int)$pLength;
		//prepare statement for editing record
		$query = $db->prepare("UPDATE $table SET TeamPool = :teamPool, PoolAddress = :pAddress, PoolLength = :pLength WHERE TeamPool = :edit1 AND PoolAddress = :edit2");
		$query->bindValue(":teamPool",$teamPool);
		$query->bindValue(":pAddress",$pAddress);
		$query->bindValue(":pLength",$pLength);
		$query->bindValue(":edit1",$edit1);
		$query->bindValue(":edit2",$edit2);
		$query->execute();
		header("Location:addDelete.php?table=$table");
	}
	//sql for deleting record
	if(isset($_POST['delete'])){
	//prepare statement for deleting record
		$query = $db->prepare("DELETE FROM $table WHERE TeamPool = :delete1 AND PoolAddress = :delete2");
		$query->bindValue(":delete1",$delete1);
		$query->bindValue(":delete2",$delete2);
		$query->execute();
		header("Location: addDelete.php?table=$table");
	}	
	

?>
