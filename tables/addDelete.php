<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../CSS/ass2style.css">
	<head>
		<title>CSC3426 Assignment 2 Matthew Stallan</title>
	</head>
	<body>
	<div id="container">
	<!-- navigation Bar -->
		<ul>
  			<li><a href="../WaterHockey.html">Home</a></li>
  			 <li class="dropdown">
   				 <a class="dropbtn">Tables</a>
    			<div class="dropdown-content">
     				<a href="addDelete.php?table=TeamTable">Team Table</a>
     				<a href="addDelete.php?table=PlayerTable">Player Table</a>
     			 	<a href="addDelete.php?table=GameTable">Game Table</a>
     			 	<a href="addDelete.php?table=PoolTable">Pool Table</a>
   				</div>
  			</li>
  			<li><a href="schema.html">Schema</a></li>
		</ul>
		<?php
			ob_start();
			$db = new SQLite3('../Ass2db.db');
			if(!array_key_exists('table',$_GET)){
			// form to select table
				echo"<h1>Which Table would you like to edit?</h1>";
				echo"<div class=\"bucket\" >";
				echo"<p><button><a href='addDelete.php?table=TeamTable'>Team Table</a></button>";
				echo"<button><a href='addDelete.php?table=PlayerTable'>Player Table</a></button>";
				echo"<button><a href='addDelete.php?table=GameTable'>Game Table</a></button>";
				echo"<button><a href='addDelete.php?table=PoolTable'>Pool Table</a></button></p>";
				echo"</div>";
			}else if( array_key_exists('table', $_GET)){
				$table = SQLite3::escapeString($_GET['table']);
				echo"<h1>$table</h1>";
				$query = "SELECT * FROM '$table';";
				$results = $db->query($query);
				$cols = $results->numColumns();
				echo "<table>";
				echo"<tr>";
				//loop to print selected table
				for($j=0; $j<$cols; $j++){
					echo"<th>".$results->columnName($j)."</th>";
				}
				echo "<tr>";
				while($row = $results->fetchArray(SQLITE3_NUM)){
					for ($i = 0; $i <= $cols+2; $i++) {
					/*for gametable since it has composite key to select correct record for 							edit delete links
					*/
						if($table == "GameTable"){
							if($i == $cols+1){
         		    			echo '<td><a href="'.$table.'.php?edit1='.$row[0].'&edit2='.$row[1].'&edit3='.$row[2].'">Edit</a></td>';	
         		    		}
         		    		if($i == $cols+2){
         		    			echo '<td><a href="'.$table.'.php?delete1='.$row[0].'&delete2='.$row[1].'&delete3='.$row[2].'">Delete</a></td>';
         		    		}
         		    		if($i < $cols){
         		    			echo"<td>";
								echo"$row[$i]";
         		    			echo"</td>"; 
         		    		}
         		    		//for pool table to select composite key for edit/delete links 
         		    	}else if($table == "PoolTable"){
         		    			if($i == $cols+1){
         		    			echo '<td><a href="'.$table.'.php?edit1='.$row[0].'&edit2='.$row[1].'">Edit</a></td>';	
         		    		}
         		    		if($i == $cols+2){
         		    			echo '<td><a href="'.$table.'.php?delete1='.$row[0].'&delete2='.$row[1].'">Delete</a></td>';
         		    		}
         		    		if($i < $cols){
         		    			echo"<td>";
								echo"$row[$i]";
         		    			echo"</td>"; 	
         		    		}
						}else{	
						// for other tables
         		    		if($i == $cols+1){
         		    			echo '<td><a href="'.$table.'.php?edit='.$row[0].'">Edit</a></td>';	
         		    		}
         		    		if($i == $cols+2){
         		    			echo '<td><a href="'.$table.'.php?delete=' .$row[0].'">Delete</a></td>';
         		    		}
         		    		if($i < $cols){
         		    			echo"<td>";
								echo"$row[$i]";
         		    			echo"</td>"; 
         		   			}
         		   		}
         		   	}
         		
         	   		echo "</tr>"; 
			
				}
				echo"</table>";
				//include selected table form for delete/edit/add
				include("$table.php");
			
			}
		
		
		?>	
		</div>
	</body>
</html>	
