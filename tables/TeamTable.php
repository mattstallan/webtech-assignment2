<style>
<?php include '../CSS/ass2style.css'; ?>
</style>

<?php
	$db = new SQLite3('../Ass2db.db');
	$table= "TeamTable";
	//check if user wants to delete row
	if(array_key_exists('delete',$_GET)){
		echo"<div id='container'>";
		$delete = SQLite3::escapeString(htmlspecialchars($_GET['delete']));
		echo"<h1>Delete Record?</h1>";
		//select record user wants to delete
		$query = "SELECT * FROM $table WHERE TeamName = '$delete'";
		$results = $db->query($query);
		$cols = $results->numColumns();
		echo "<table>";
		echo"<tr>";
		//display record user wants to delete
		for($j=0; $j<$cols; $j++){
			echo"<th>".$results->columnName($j)."</th>";
		}
		echo "<tr>";
		while($row = $results->fetchArray(SQLITE3_NUM)){
			for ($i = 0; $i <$cols; $i++) {
            		echo"<td>";
					echo"$row[$i]";
            		echo"</td>"; 
           		}
         	   	
         		
        	echo "</tr>"; 
			
		}
		echo"</table>";
		echo"<div class=\"bucket\">";
		// form for confirming deletion or canceling
		echo"<p class=\"center\">Are you sure you want to delete this record?</p>";
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<button class=\"left\" type=\"submit\" name=\"delete\" value=\"Submit\">Yes</button>";
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";
		echo"</div>";
	}
	// check if user wants to edit record
	if(array_key_exists('edit',$_GET)){
		echo"<div id='container'>";
		$edit = SQLite3::escapeString($_GET['edit']);
		//select record user wants to edit
		$query = "SELECT * FROM $table WHERE TeamName = '$edit'";
		$results = $db->query($query);
		$cols = $results->numColumns();
		$row = $results->fetchArray(SQLITE3_NUM);
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<h1>Edit Team</h1>";
		echo"<div class=\"bucket\">";
		// display form with selected records data to allow editing or cancel
		for($i=0;$i<$cols;$i++){
			echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\"".$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'/></label></p>";
		}
		echo"<button class=\"left\" type=\"submit\" name=\"edit\" value=\"Submit\">Edit</button>";
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";	
		echo"</div>";
		
	}else if(!array_key_exists('edit',$_GET) && !array_key_exists('delete',$_GET)){	
		//form to add new record to table
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<div class=\"bucket\">";
		echo"<h3>Add New Team</h3>";
		for($i=0;$i<$cols;$i++){
			echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\"".$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'/></label></p>";
		}
		echo"<button class=\"add\" type=\"submit\" name=\"add\" value=\"Submit\">Add</button>";
		echo"</form>";
		echo"</div>";
	}
	//sql for adding new record
	if (isset($_POST['add'])){
		//get data for new record
		$teamName = SQLite3::escapeString(htmlspecialchars($_POST['TeamName']));
		$manager =  SQLite3::escapeString(htmlspecialchars($_POST['Manager']));
		$teamPool =  SQLite3::escapeString(htmlspecialchars($_POST['TeamPool']));
		// prepare statement for adding new record
		$query = $db->prepare("INSERT INTO TeamTable(TeamName,Manager,TeamPool)
								VALUES(:teamName,:manager,:teamPool)");
		$query->bindValue(":teamName",$teamName);
		$query->bindValue(":manager",$manager);
		$query->bindValue(":teamPool",$teamPool);
		$query->execute();
		
		header("Location: addDelete.php?table=$table");
	}
	
	//sql for editing record
	if(isset($_POST['edit'])){
		//get data for edited record
		$teamName = SQLite3::escapeString(htmlspecialchars($_POST['TeamName']));
		$manager =  SQLite3::escapeString(htmlspecialchars($_POST['Manager']));
		$teamPool =  SQLite3::escapeString(htmlspecialchars($_POST['TeamPool']));
		//prepare statement for editing record
		$query = $db->prepare("UPDATE TeamTable SET TeamName = :teamName, Manager = :manager, TeamPool 									= :teamPool WHERE TeamName = :edit");
		$query->bindValue(":teamName",$teamName);
		$query->bindValue(":manager",$manager);
		$query->bindValue(":teamPool",$teamPool);
		$query->bindValue(":edit",$edit);
		$query->execute();
		header("Location: addDelete.php?table=$table");
	}
	//sql for deleting record
	if(isset($_POST['delete'])){
		//prepare statement for deleting record
		$query = $db->prepare("DELETE FROM TeamTable WHERE teamName = :delete");
		$query->bindValue(":delete",$delete);
		$query->execute();
		header("Location: addDelete.php?table=$table");
	}
?>

