<style>
<?php include '../CSS/ass2style.css'; ?>
</style>
<?php

	$db = new SQLite3('../Ass2db.db');
	$table= "GameTable";
	//check if user wants to delete row
	if(array_key_exists('delete1',$_GET)&&array_key_exists('delete2',$_GET)&&array_key_exists('delete3',$_GET)){
		echo"<div id='container'>";
		$delete1 = SQLite3::escapeString($_GET['delete1']);
		$delete2 = SQLite3::escapeString($_GET['delete2']);
		$delete3 = SQLite3::escapeString($_GET['delete3']);
		echo"<h1>Delete Record?</h1>";
		//select record user wants to delete
		$query = "SELECT * FROM $table WHERE TeamA = '$delete1' AND TeamB = '$delete2' AND DateOfGame = '$delete3'";
		$results = $db->query($query);
		$cols = $results->numColumns();
		echo "<table>";
		echo"<tr>";
		//display record user wants to delete
		for($j=0; $j<$cols; $j++){
			echo"<th>".$results->columnName($j)."</th>";
		}
		echo "<tr>";
		while($row = $results->fetchArray(SQLITE3_NUM)){
			for ($i = 0; $i <$cols; $i++) {
            		echo"<td>";
					echo"$row[$i]";
            		echo"</td>"; 
           		}
         	   	
         		
        	echo "</tr>"; 
			
				}
			echo"</table>";
		echo"<div class=\"bucket\">";
		// form for confirming deletion or canceling
		echo"<p class=\"center\">Are you sure you want to delete this record?</p>";
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<button class=\"left\" type=\"submit\" name=\"delete\" value=\"Submit\">Yes</button>";
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";
		echo"</div>";
	}
	// check if user wants to edit record
	if(array_key_exists('edit1',$_GET)&&array_key_exists('edit2',$_GET)&&array_key_exists('edit3',$_GET)){
		echo"<div id='container'>";
		$edit1 = SQLite3::escapeString($_GET['edit1']);
		$edit2 = SQLite3::escapeString($_GET['edit2']);
		$edit3 = SQLite3::escapeString($_GET['edit3']);
		//select record user wants to edit
		$query = "SELECT * FROM $table WHERE TeamA = '$edit1' AND TeamB = '$edit2' AND DateOfGame = '$edit3'";	
		$results = $db->query($query);
		$cols = $results->numColumns();
		$row = $results->fetchArray(SQLITE3_NUM);
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<h1>Edit Game</h1>";
		echo"<div class=\"bucket\">";
		// display form with selected records data to allow editing or cancel
		for($i=0;$i<$cols;$i++){
			if($results->columnName($i) == "TeamAScore"
			||$results->columnName($i) == "TeamBScore"){
				echo"<p><label>".$results->columnName($i).": <input type=\"number\" name=\"".$results->columnName($i)."\" value=\"".$row[$i]."\"required='required' pattern=\"[0-9]+\"/></label></p>";
			}else{
				echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\""
				.$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'/>
				</label></p>";
			}
		}
		echo"<button class=\"left\" type=\"submit\" name=\"edit\" value=\"Submit\">Edit</button>";
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";	
		echo"</div>";	
	}else if(!(array_key_exists('edit1',$_GET)||array_key_exists('edit2',$_GET)||array_key_exists('edit3',$_GET)) && !(array_key_exists('delete1',$_GET)||array_key_exists('delete2',$_GET)||array_key_exists('delete3',$_GET))){	
		//form to add new record to table
		$query = "SELECT * FROM $table";
		$results = $db->query($query);
		$cols = $results->numColumns();
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<div class=\"bucket\">";
		echo"<h3>Add New Game</h3>";
		for($i=0;$i<$cols;$i++){
			if($results->columnName($i) == "TeamAScore"
			||$results->columnName($i) == "TeamBScore"){
				echo"<p><label>".$results->columnName($i).": <input type=\"number\" name=\""
				.$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'
				 pattern=\"[0-9]+\"/></label></p>";
			}else{
				echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\""
				.$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'/>
				</label></p>";
			}
		}
		echo"<button class=\"add\" type=\"submit\" name=\"add\" value=\"Submit\">Add</button>";
		echo"</form>";
		echo"</div>";
	}
	//sql for adding new record
	if (isset($_POST['add'])){
	//get data for new record
		$teamA = SQLite3::escapeString(htmlspecialchars($_POST['TeamA']));
		$teamB = SQLite3::escapeString(htmlspecialchars($_POST['TeamB']));
		$gdate = SQLite3::escapeString(htmlspecialchars($_POST['DateOfGame']));
		$tAScore = SQLite3::escapeString(htmlspecialchars($_POST['TeamAScore']));
		$tBScore = SQLite3::escapeString(htmlspecialchars($_POST['TeamBScore']));
		$gpool = SQLite3::escapeString(htmlspecialchars($_POST['GamePool']));
		// prepare statement for adding new record
		$query = $db->prepare("INSERT INTO $table(TeamA,TeamB,DateOfGame,TeamAScore,TeamBScore,GamePool)
								VALUES(:teamA,:teamB,:gdate,:tAScore,:tBScore,:gpool)");
		$query->bindValue(":teamA",$teamA);
		$query->bindValue(":teamB",$teamB);
		$query->bindValue(":gdate",$gdate);
		$query->bindValue(":tAScore",$tAScore);
		$query->bindValue(":tBScore",$tBScore);
		$query->bindValue(":gpool",$gpool);
		$query->execute();
		header("Location:addDelete.php?table=$table");
	}
	//sql for editing record
	if(isset($_POST['edit'])){
	//get data for edited record
		$teamA = SQLite3::escapeString(htmlspecialchars($_POST['TeamA']));
		$teamB = SQLite3::escapeString(htmlspecialchars($_POST['TeamB']));
		$gdate = SQLite3::escapeString(htmlspecialchars($_POST['DateOfGame']));
		$tAScore = SQLite3::escapeString(htmlspecialchars($_POST['TeamAScore']));
		$tBScore = SQLite3::escapeString(htmlspecialchars($_POST['TeamBScore']));
		$gpool = SQLite3::escapeString(htmlspecialchars($_POST['GamePool']));
		//prepare statement for editing record
		$query = $db->prepare("UPDATE $table SET TeamA = :teamA, TeamB = :teamB, DateOfGame = :gdate, TeamAScore = :tAScore,TeamBScore = :tBScore,GamePool = :gpool WHERE TeamA = :edit1 AND TeamB = :edit2 AND DateOfGame = :edit3");
		$query->bindValue(":teamA",$teamA);
		$query->bindValue(":teamB",$teamB);
		$query->bindValue(":gdate",$gdate);
		$query->bindValue(":tAScore",$tAScore);
		$query->bindValue(":tBScore",$tBScore);
		$query->bindValue(":gpool",$gpool);
		$query->bindValue(":edit1",$edit1);
		$query->bindValue(":edit2",$edit2);
		$query->bindValue(":edit3",$edit3);
		$query->execute();
		header("Location:addDelete.php?table=$table");
		
	}
	//sql for deleting record
	if(isset($_POST['delete'])){
		//prepare statement for deleting record
		$query = $db->prepare("DELETE FROM $table WHERE TeamA = :delete1 AND TeamB = :delete2 AND DateOfGame = :delete3");
		$query->bindValue(":delete1",$delete1);
		$query->bindValue(":delete2",$delete2);
		$query->bindValue(":delete3",$delete3);
		$query->execute();
		header("Location: addDelete.php?table=$table");
	}	
?>
