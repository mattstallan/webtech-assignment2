<style>
<?php include '../CSS/ass2style.css'; ?>
</style>
<?php

	$db = new SQLite3('../Ass2db.db');
	$table= "PlayerTable";
	//check if user wants to delete row
	if(array_key_exists('delete',$_GET)){
		echo"<div id='container'>";
		$delete = SQLite3::escapeString($_GET['delete']);
		echo"<h1>Delete Record?</h1>";
		//select record user wants to delete
		$query = "SELECT * FROM $table WHERE PID = '$delete'";
		$results = $db->query($query);
		$cols = $results->numColumns();
		echo "<table>";
		echo"<tr>";
		//display record user wants to delete
		for($j=0; $j<$cols; $j++){
			echo"<th>".$results->columnName($j)."</th>";
		}
		echo "<tr>";
		while($row = $results->fetchArray(SQLITE3_NUM)){
			for ($i = 0; $i <$cols; $i++) {
            		echo"<td>";
					echo"$row[$i]";
            		echo"</td>"; 
           		}
         	   	
         		
        	echo "</tr>"; 
			
				}
			
		echo"</table>";
		echo"<div class=\"bucket\">";
		// form for confirming deletion or canceling
		echo"<p class=\"center\">Are you sure you want to delete this record?</p>";
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<button class=\"left\" type=\"submit\" name=\"delete\" value=\"Submit\">Yes</button>";
		
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";
		echo"</div>";
	}
	// check if user wants to edit record
	if(array_key_exists('edit',$_GET)){
		echo"<div id='container'>";
		$edit = SQLite3::escapeString($_GET['edit']);
		//select record user wants to edit
		$query = "SELECT * FROM $table WHERE PID = '$edit'";
		$results = $db->query($query);
		$row = $results->fetchArray(SQLITE3_NUM);
		$cols = $results->numColumns();
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<h1>Edit Player</h1>";
		echo"<div class=\"bucket\">";
		// display form with selected records data to allow editing or cancel
		for($i=0;$i<$cols;$i++){
			echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\"".$results->columnName($i)."\" value=\"".$row[$i]."\"/required='required'></label></p>";
		}
		echo"<button class=\"left\" type=\"submit\" name=\"edit\" value=\"Submit\">Edit</button>";
		echo"<button class=\"right\"><a href='addDelete.php?table=$table'>Cancel</a></button>";
		echo"</form>";
		echo"</div>";	
		echo"</div>";
			
	}else if(!array_key_exists('edit',$_GET) && !array_key_exists('delete',$_GET)){	
	//form to add new record to table
		$query = "SELECT * FROM $table";
		$results = $db->query($query);
		$cols = $results->numColumns();
		echo"<form id=\"add\" method=\"POST\" action=\"\">";
		echo"<div class=\"bucket\">";
		echo"<h3>Add New Player</h3>";
		for($i=1;$i<$cols;$i++){
			echo"<p><label>".$results->columnName($i).": <input type=\"text\" name=\"".$results->columnName($i)."\" value=\"".$row[$i]."\"required='required'/></label></p>";
		}
		echo"<button class=\"add\" type=\"submit\" name=\"add\" value=\"Submit\">Add</button>";
		echo"</form>";
		echo"</div>";
	}
	//sql for adding new record
	if (isset($_POST['add'])){
	//get data for new record
		$pfname = SQLite3::escapeString(htmlspecialchars($_POST['PlayerGivenName']));
		$plname = SQLite3::escapeString(htmlspecialchars($_POST['PlayerLastName']));
		$pdob = SQLite3::escapeString(htmlspecialchars($_POST['PlayerDoB']));
		$phand = SQLite3::escapeString(htmlspecialchars($_POST['Handed']));
 		$pteam = SQLite3::escapeString(htmlspecialchars($_POST['TeamName']));
 		// prepare statement for adding new record
 		$query = $db->prepare("INSERT INTO PlayerTable(PlayerGivenName,PlayerLastName,PlayerDoB,Handed,TeamName)
								VALUES(:pfname,:plname,:pdob,:phand,:pteam)");
		$query->bindValue(":pfname",$pfname);
		$query->bindValue(":plname",$plname);
		$query->bindValue(":pdob",$pdob);
		$query->bindValue(":phand",$phand);
		$query->bindValue(":pteam",$pteam);
		$query->execute();
		header("Location:addDelete.php?table=$table");
	}
	//sql for editing record
	if(isset($_POST['edit'])){
	//get data for edited record
		$pfname = SQLite3::escapeString(htmlspecialchars($_POST['PlayerGivenName']));
		$plname = SQLite3::escapeString(htmlspecialchars($_POST['PlayerLastName']));
		$pdob = SQLite3::escapeString(htmlspecialchars($_POST['PlayerDoB']));
		$phand = SQLite3::escapeString(htmlspecialchars($_POST['Handed']));
 		$pteam = SQLite3::escapeString(htmlspecialchars($_POST['TeamName']));
 		//prepare statement for editing record
 		$query = $db->prepare("UPDATE $table SET PlayerGivenName = :pfname,PlayerLastName = :plname, PlayerDob = :pdob, 									Handed = :phand, TeamName = :pteam WHERE PID = :edit");
		$query->bindValue(":pfname",$pfname);
		$query->bindValue(":plname",$plname);
		$query->bindValue(":pdob",$pdob);
		$query->bindValue(":phand",$phand);
		$query->bindValue(":pteam",$pteam);
		$query->bindValue(":edit",$edit);
		$query->execute();
		header("Location:addDelete.php?table=$table");
	} 		
	//sql for deleting record
 	if(isset($_POST['delete'])){
 	//prepare statement for deleting record
		$query = $db->prepare("DELETE FROM $table WHERE PID = :delete");
		$query->bindValue(":delete",$delete);
		$query->execute();
		header("Location: addDelete.php?table=$table");
	}	
 		
 		
 		
?>
	
	
	
	
	
	
	
	
	
	
		
